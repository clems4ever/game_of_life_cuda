//
// Created by clement on 26/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_GAMEOFLIFEPLAYERVIEW_HPP
#define GAME_OF_LIFE_CUDA_GAMEOFLIFEPLAYERVIEW_HPP

#include <QtOpenGL/QtOpenGL>
#include "Controller/IGameOfLifeRenderer.hpp"

class PlayerSurfaceWidget : public QOpenGLWidget, QGLFunctions
{
    Q_OBJECT
public:
    PlayerSurfaceWidget(IGameOfLifeRenderer &renderer, QWidget *parent = 0);
    virtual ~PlayerSurfaceWidget();

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

private:
    IGameOfLifeRenderer &m_renderer;
};


#endif //GAME_OF_LIFE_CUDA_GAMEOFLIFEPLAYERVIEW_HPP
