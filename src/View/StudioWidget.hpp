//
// Created by clement on 26/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_STUDIOWIDGET_H
#define GAME_OF_LIFE_CUDA_STUDIOWIDGET_H

#include <memory>

#include <QWidget>

#include "Controller/IGameOfLifePlayer.hpp"

#include "View/PlayerSurfaceWidget.hpp"
#include "View/PlayerControllerWidget.hpp"

#include "KernelEditorWidget.hpp"

class StudioWidget : public QWidget
{
public:
    StudioWidget(IGameOfLifeRenderer &gameOfLifeRenderer, const std::string &defaultEditorSourceCode, QWidget *parent = 0);
    virtual ~StudioWidget();

    void resizeSurface(unsigned int width, unsigned int height);
    void updateSurface();

    PlayerSurfaceWidget &playerSurfaceWidget()
    {
        return m_playerSurfaceWidget;
    }

    PlayerControllerWidget &playerControllerWidget()
    {
        return m_playerControllerWidget;
    }

    KernelEditorWidget &getKernelEditorWidget()
    {
        return m_kernelEditorWidget;
    }

private:
    PlayerSurfaceWidget m_playerSurfaceWidget;
    PlayerControllerWidget m_playerControllerWidget;

    KernelEditorWidget m_kernelEditorWidget;
};


#endif //GAME_OF_LIFE_CUDA_GAMEOFLIFESTUDIO_H
