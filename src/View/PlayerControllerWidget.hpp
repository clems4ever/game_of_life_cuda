//
// Created by clement on 27/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_PLAYERCONTROLLERWIDGET_HPP
#define GAME_OF_LIFE_CUDA_PLAYERCONTROLLERWIDGET_HPP

#include <QWidget>

#include <QPushButton>
#include <QTextEdit>
#include <QLabel>

class PlayerControllerWidget : public QWidget
{
    Q_OBJECT

public:
    PlayerControllerWidget(QWidget *parent = 0);

    void setNextIterationButtonEnabled(bool enabled);

signals:
    void onRandomizeButtonClicked();
    void onNextIterationButtonClicked();
    void onPlayPauseButtonClicked();

    void onApplyKernelButtonClicked();

private:
    QPushButton m_randomizeButton;
    QPushButton m_nextIterationButton;
    QPushButton m_playPauseButton;
    QPushButton m_applyKernelButton;
};


#endif //GAME_OF_LIFE_CUDA_PLAYERCONTROLLERWIDGET_HPP
