//
// Created by clement on 28/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_KERNELEDITORWIDGET_HPP
#define GAME_OF_LIFE_CUDA_KERNELEDITORWIDGET_HPP

#include <memory>

#include <QTextEdit>
#include <QPushButton>
#include <QLabel>

#include <QWidget>

#include "Utils/ICudaUpdateKernel.hpp"

class KernelEditorWidget : public QWidget
{
    Q_OBJECT
public:
    KernelEditorWidget(QWidget *parent = 0);

    void setSourceCode(const std::string &sourceCode);

    ICudaUpdateKernel *kernel() const;

public slots:
    void compile();

private:
    QTextEdit m_kernelSourceCodeTextEdit;
    QPushButton m_compilePushButton;
    QLabel m_logsLabel;

    std::unique_ptr<ICudaUpdateKernel> m_cudaKernel;
};


#endif //GAME_OF_LIFE_CUDA_KERNELEDITORWIDGET_HPP
