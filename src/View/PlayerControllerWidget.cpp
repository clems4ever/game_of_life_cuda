//
// Created by clement on 27/03/16.
//

#include "PlayerControllerWidget.hpp"

#include <QHBoxLayout>

PlayerControllerWidget::PlayerControllerWidget(QWidget *parent)
: QWidget(parent), m_randomizeButton("Randomize"), m_nextIterationButton("Step"), m_playPauseButton("Play/Pause"), m_applyKernelButton("Apply Kernel")
{
    //QVBoxLayout *mainLayout = new QVBoxLayout();

    QHBoxLayout *buttonsLayout = new QHBoxLayout();

    buttonsLayout->addWidget(&m_randomizeButton);
    buttonsLayout->addWidget(&m_nextIterationButton);
    buttonsLayout->addWidget(&m_playPauseButton);
    buttonsLayout->addWidget(&m_applyKernelButton);

    this->setLayout(buttonsLayout);

    connect(&m_randomizeButton, SIGNAL(clicked()), this, SIGNAL(onRandomizeButtonClicked()));
    connect(&m_nextIterationButton, SIGNAL(clicked()), this, SIGNAL(onNextIterationButtonClicked()));
    connect(&m_playPauseButton, SIGNAL(clicked()), this, SIGNAL(onPlayPauseButtonClicked()));

    connect(&m_applyKernelButton, SIGNAL(clicked()), this, SIGNAL(onApplyKernelButtonClicked()));
}

void PlayerControllerWidget::setNextIterationButtonEnabled(bool enabled)
{
    m_nextIterationButton.setEnabled(enabled);
}