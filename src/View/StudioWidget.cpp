//
// Created by clement on 26/03/16.
//
#include "StudioWidget.hpp"

StudioWidget::StudioWidget(IGameOfLifeRenderer &gameOfLifeRenderer, const std::string &defaultEditorSourceCode, QWidget *parent)
: QWidget(parent), m_playerSurfaceWidget(gameOfLifeRenderer)
{
    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_playerSurfaceWidget.setSizePolicy(sizePolicy);

    QVBoxLayout *playerLayout = new QVBoxLayout();
    playerLayout->addWidget(&m_playerSurfaceWidget);
    playerLayout->addWidget(&m_playerControllerWidget);

    QHBoxLayout *mainLayout = new QHBoxLayout();

    mainLayout->addLayout(playerLayout);
    mainLayout->addWidget(&m_kernelEditorWidget);

    m_kernelEditorWidget.setSourceCode(defaultEditorSourceCode);
    m_kernelEditorWidget.compile();

    mainLayout->setMargin(0);
    setLayout(mainLayout);
}


StudioWidget::~StudioWidget()
{
}

void StudioWidget::resizeSurface(unsigned int width, unsigned int height)
{
    m_playerSurfaceWidget.setMinimumSize(QSize(width, height));
}

void StudioWidget::updateSurface()
{
    m_playerSurfaceWidget.update();
}