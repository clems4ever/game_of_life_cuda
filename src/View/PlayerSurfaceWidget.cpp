//
// Created by clement on 26/03/16.
//

#include "PlayerSurfaceWidget.hpp"

PlayerSurfaceWidget::PlayerSurfaceWidget(IGameOfLifeRenderer &renderer, QWidget *parent)
: QOpenGLWidget(parent), m_renderer(renderer)
{
}

PlayerSurfaceWidget::~PlayerSurfaceWidget()
{
    m_renderer.cleanup();
}

static void resizeViewPort(int width, int height)
{
    glViewport (0, 0, (GLsizei) width , (GLsizei) height);
}

void PlayerSurfaceWidget::initializeGL()
{
    initializeGLFunctions();

    resizeViewPort(this->width(), this->height());
    glClearColor(0.0, 0.0, 0.0, 1.0);

    glDisable(GL_DEPTH_TEST);

    // set view matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f);

    m_renderer.initialize();
}

void PlayerSurfaceWidget::paintGL()
{
    m_renderer.render();
}

void PlayerSurfaceWidget::resizeGL(int width, int height)
{
    resizeViewPort(width, height);
    glClearColor(0.0, 0.0, 0.0, 1.0);
}