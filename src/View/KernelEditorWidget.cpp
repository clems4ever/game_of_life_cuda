//
// Created by clement on 28/03/16.
//

#include "KernelEditorWidget.hpp"

#include "Utils/CudaKernelCompiler.hpp"
#include "Utils/DynamicCudaUpdateKernel.hpp"

#include <QVBoxLayout>

KernelEditorWidget::KernelEditorWidget(QWidget *parent)
: QWidget(parent), m_compilePushButton("Compile")
{
    QVBoxLayout *mainLayout = new QVBoxLayout();

    mainLayout->addWidget(&m_kernelSourceCodeTextEdit);
    mainLayout->addWidget(&m_compilePushButton);
    mainLayout->addWidget(&m_logsLabel);

    setLayout(mainLayout);

    connect(&m_compilePushButton, SIGNAL(clicked()), this, SLOT(compile()));
}

void KernelEditorWidget::setSourceCode(const std::string &sourceCode)
{
    m_kernelSourceCodeTextEdit.setPlainText(QString::fromStdString(sourceCode));
}

ICudaUpdateKernel *KernelEditorWidget::kernel() const
{
    return m_cudaKernel.get();
}

void KernelEditorWidget::compile()
{
    CudaKernelCompiler compiler;
    std::string logs;

    CudaProgram program = compiler.compile(m_kernelSourceCodeTextEdit.toPlainText().toStdString(), "program", logs);

    m_logsLabel.setText(QString::fromStdString(logs));

    m_cudaKernel = std::unique_ptr<ICudaUpdateKernel>(new DynamicCudaUpdateKernel(program, "update_game_of_life_kernel"));
}