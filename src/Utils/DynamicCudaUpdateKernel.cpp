//
// Created by clement on 28/03/16.
//

#include "DynamicCudaUpdateKernel.hpp"

#include <cuda.h>

DynamicCudaUpdateKernel::DynamicCudaUpdateKernel(const CudaProgram program, const std::string &functionName)
: m_kernel(0)
{
    CUmodule module = { 0 };

    cuModuleLoadDataEx(&module, program, 0, 0, 0);
    cuModuleGetFunction(&m_kernel, module, functionName.c_str()); // Generate input for execution, and create output buffers.*/
}

DynamicCudaUpdateKernel::DynamicCudaUpdateKernel(const DynamicCudaUpdateKernel &that)
: m_kernel(that.m_kernel)
{
}

void DynamicCudaUpdateKernel::run(CudaDim3 dimBlocks, CudaDim3 dimThreads, char *gof1, char *gof2)
{
    void *args[] = { &gof1, &gof2 };

    cuLaunchKernel(m_kernel, dimBlocks.x, dimBlocks.y, dimBlocks.z,
                   dimThreads.x, dimThreads.y, dimThreads.z,
                   0, NULL, args, 0);

    cuCtxSynchronize();
}

std::unique_ptr<ICudaUpdateKernel> DynamicCudaUpdateKernel::clone()
{
    return std::unique_ptr<ICudaUpdateKernel>(new DynamicCudaUpdateKernel(*this));
}
