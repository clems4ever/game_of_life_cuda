//
// Created by clement on 28/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_CUDAKERNELEXECUTOR_HPP
#define GAME_OF_LIFE_CUDA_CUDAKERNELEXECUTOR_HPP

#include "CudaTypes.hpp"

#include <string>
#include "ICudaUpdateKernel.hpp"

class DynamicCudaUpdateKernel : public ICudaUpdateKernel
{
public:
    DynamicCudaUpdateKernel(const CudaProgram program, const std::string &functionName);
    DynamicCudaUpdateKernel(const DynamicCudaUpdateKernel &that);

    void run(CudaDim3 dimBlocks, CudaDim3 dimThreads, char *gof1, char *gof2);

    std::unique_ptr<ICudaUpdateKernel> clone();

private:
    CUfunction m_kernel;
};


#endif //GAME_OF_LIFE_CUDA_CUDAKERNELEXECUTOR_HPP
