//
// Created by clement on 28/03/16.
//

#include "CudaKernelCompiler.hpp"

#include <nvrtc.h>

#include <cuda.h>
#include <stdexcept>

void getLogs(nvrtcProgram prog, std::string &logs)
{
    // Obtain compilation log from the program.
    size_t logSize = 0;
    nvrtcGetProgramLogSize(prog, &logSize);

    if(logSize > 0)
    {
        char *mylog = new char[logSize];
        nvrtcGetProgramLog(prog, mylog);
        logs = mylog;
        delete mylog;
    }
}

CudaProgram CudaKernelCompiler::compile(const std::string &sourceCode, const std::string &programName, std::string &logs) const
{
    nvrtcProgram prog;
    nvrtcCreateProgram(&prog, sourceCode.c_str(), programName.c_str(), 0, NULL, NULL);

    const char *opts[] = {"--gpu-architecture=compute_20", "--fmad=false"};
    nvrtcResult compileResult = nvrtcCompileProgram(prog, 2, opts);

    try
    {
        getLogs(prog, logs);
    }
    catch(std::logic_error e)
    {
    }

    if (compileResult != NVRTC_SUCCESS)
    {
        return NULL;
    }

    size_t ptxSize;
    nvrtcGetPTXSize(prog, &ptxSize);
    CudaProgram ptx = new char[ptxSize];
    nvrtcGetPTX(prog, ptx);
    nvrtcDestroyProgram(&prog);

    return ptx;
}
