//
// Created by clement on 27/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_IRANDOMNUMBERGENERATOR_HPP
#define GAME_OF_LIFE_CUDA_IRANDOMNUMBERGENERATOR_HPP

class IRandomNumberGenerator
{
public:
    virtual int pick(int max_value) const = 0;
};


#endif //GAME_OF_LIFE_CUDA_IRANDOMNUMBERGENERATOR_HPP
