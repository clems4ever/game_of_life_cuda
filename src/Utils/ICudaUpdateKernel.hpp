//
// Created by clement on 28/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_ICUDAKERNEL_HPP
#define GAME_OF_LIFE_CUDA_ICUDAKERNEL_HPP

#include <memory>

#include "CudaTypes.hpp"

class ICudaUpdateKernel
{
public:
    virtual std::unique_ptr<ICudaUpdateKernel> clone() = 0;

    virtual void run(CudaDim3 dimBlocks, CudaDim3 dimThreads, char *gof1, char *gof2) = 0;
};


#endif //GAME_OF_LIFE_CUDA_ICUDAKERNEL_HPP
