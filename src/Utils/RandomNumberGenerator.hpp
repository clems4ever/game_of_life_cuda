//
// Created by clement on 27/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_RANDOMNUMBERGENERATOR_HPP
#define GAME_OF_LIFE_CUDA_RANDOMNUMBERGENERATOR_HPP

#include "IRandomNumberGenerator.hpp"

class RandomNumberGenerator : public IRandomNumberGenerator
{
public:
    int pick(int max_value) const
    {
        return rand() % max_value;
    }
};

#endif //GAME_OF_LIFE_CUDA_RANDOMNUMBERGENERATOR_HPP
