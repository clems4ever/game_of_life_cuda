//
// Created by clement on 28/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_CUDATYPES_HPP
#define GAME_OF_LIFE_CUDA_CUDATYPES_HPP

#include <cuda.h>

typedef char* CudaProgram;

typedef CUfunction CudaKernel;

typedef struct CudaDim3
{
    int x;
    int y;
    int z;
} CudaDim3;

#endif //GAME_OF_LIFE_CUDA_CUDATYPES_HPP
