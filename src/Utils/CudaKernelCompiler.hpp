//
// Created by clement on 28/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_CUDAKERNELCOMPILER_HPP
#define GAME_OF_LIFE_CUDA_CUDAKERNELCOMPILER_HPP

#include <string>
#include <vector>

#include "CudaTypes.hpp"

class CudaKernelCompiler
{
public:

    CudaProgram compile(const std::string &sourceCode, const std::string &programName, std::string &logs) const;
};


#endif //GAME_OF_LIFE_CUDA_CUDAKERNELCOMPILER_HPP
