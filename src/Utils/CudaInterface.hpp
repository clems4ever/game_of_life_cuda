//
// Created by clement on 27/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_CUDAINTERFACE_HPP
#define GAME_OF_LIFE_CUDA_CUDAINTERFACE_HPP

#include <cuda.h>

class CudaInterface
{
public:
    static void setGLDevice(int device)
    {
        cudaGLSetGLDevice(device);
    }

    static void malloc(void **devPtr, size_t size)
    {
        cudaMalloc(devPtr, size);
    }

    static void free(void *devPtr)
    {
        cudaFree(devPtr);
    }

    static void memcpy(void * dst, const void * src, size_t count, enum cudaMemcpyKind kind)
    {
        cudaMemcpy(dst, src, count, kind);
    }

    static void mapBufferObject(void ** devPtr, GLuint bufObj)
    {
        cudaGLMapBufferObject(devPtr, bufObj);
    }

    static void unmapBufferObject(GLuint bufObj)
    {
        cudaGLUnmapBufferObject(bufObj);
    }

    static void registerBufferObject(GLuint bufObj)
    {
        cudaGLRegisterBufferObject(bufObj);
    }

    static void unregisterBufferObject(GLuint bufObj)
    {
        cudaGLUnregisterBufferObject(bufObj);
    }

    static void launchKernel(CudaKernel kernel, void **args, dim3 dimBlocks, dim3 dimThreads)
    {
        cuLaunchKernel(kernel, dimBlocks.x, dimBlocks.y, dimBlocks.z, // grid dim
                       dimThreads.x, dimThreads.y, dimThreads.z, // block dim
                       0, NULL, // shared mem and stream
                       args, 0); // arguments
    }
};

#endif //GAME_OF_LIFE_CUDA_CUDAINTERFACE_HPP
