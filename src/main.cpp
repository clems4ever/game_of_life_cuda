
#include <iostream>

#include <memory>
#include <unistd.h>

#include <QtWidgets/QApplication>

#include "Controller/GameOfLifeUpdaterFullGpu.hpp"
#include "Controller/StudioController.hpp"

#include "Utils/CudaInterface.hpp"

#define WINDOW_WIDTH 512 
#define WINDOW_HEIGHT 512 

#define CELL_SIZE 1

std::string globalDefaultUpdateKernelSourceCode = ""
        "#define RADIUS 1\n\n"
        "extern \"C\" __global__ void update_game_of_life_kernel(const char *gof1, char *gof2)\n"
        "{\n"
        "\tint x = blockIdx.x;\n"
        "\tint y = blockIdx.y;\n"
        "\tint width = gridDim.x;\n"
        "\n"
        "\tint gindex = y * width + x; \n"
        "\n"
        "\tint sum = 0;\n"
        "\tfor(int i=-RADIUS; i<=RADIUS; ++i)\n"
        "\t{\n"
        "\t\tfor(int j=-RADIUS; j<=RADIUS; ++j)\n"
        "\t\t{\n"
        "\t\t\t// if in bound\n"
        "\t\t\tif(x+i >= 0 && x+i < gridDim.x && y+j >= 0 && y+j < gridDim.y)\n"
        "\t\t\t{\n"
        "\t\t\t\tif((i != 0 || j != 0) && gof1[gindex + j * width + i]) sum++;\n"
        "\t\t\t}\n"
        "\t\t}\n"
        "\t}\n"
        "\tint live = (gof1[gindex] && (sum >= 2 && sum <= 3)) || (!gof1[gindex] && sum == 3) ? 1 : 0;\n"
        "\tgof2[gindex] = live;\n"
        "}\n";


// bridge to static kernel launchers
void runRenderKernel(uchar4 *data, char *cudaGameOfLife, int width, int height);
void runUpdateKernel(char *cudaGof1, char *cudaGof2, int width, int height);

namespace
{
    class RenderKernel
    {
    public:
        static void run(uchar4 *data, char *cudaGameOfLife, int width, int height)
        {
            runRenderKernel(data, cudaGameOfLife, width, height);
        }
    };

    class UpdateKernel : public ICudaUpdateKernel
    {
    public:
        void run(CudaDim3 blocks, CudaDim3 threads, char *gof1, char *gof2)
        {
            runUpdateKernel(gof1, gof2, blocks.x, blocks.y);
        }

        std::unique_ptr<ICudaUpdateKernel> clone()
        {
            return std::unique_ptr<ICudaUpdateKernel>(new UpdateKernel());
        }
    };

    typedef GameOfLifeUpdaterFullGpu<CudaInterface, RenderKernel> CudaUpdater;
}

int main(int argc, char **argv)
{
	srand(time(NULL));

    QApplication app(argc, argv);
    std::unique_ptr<CudaUpdater> gameOfLifeUpdaterFullGpu(new CudaUpdater(WINDOW_WIDTH, WINDOW_HEIGHT, CELL_SIZE));

    gameOfLifeUpdaterFullGpu->setUpdateKernel(std::unique_ptr<ICudaUpdateKernel>(new UpdateKernel()));

    StudioController studio(std::move(gameOfLifeUpdaterFullGpu), globalDefaultUpdateKernelSourceCode);

    studio.start();

    return app.exec();
}
