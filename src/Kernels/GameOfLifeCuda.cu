#include "../Model/GameOfLife.hpp"

#include <cuda.h>

#include <iostream>

#define RADIUS 1

void updateWithCuda(const GameOfLife &gameOfLife1, GameOfLife &gameOfLife2);

void checkCUDAError(const char *msg)
{
  cudaError_t err = cudaGetLastError();
  if( cudaSuccess != err) {
    fprintf(stderr, "Cuda error: %s: %s.\n", msg, cudaGetErrorString( err) ); 
    exit(EXIT_FAILURE); 
  }
}

__global__ void update_game_of_life_kernel(const char *gof1, char *gof2)
{
	int x = blockIdx.x;
	int y = blockIdx.y;
	int width = gridDim.x;

	int gindex = y * width + x; 

	int sum = 0;
	for(int i=-RADIUS; i<=RADIUS; ++i)
	{
		for(int j=-RADIUS; j<=RADIUS; ++j)
		{
			// if in bound
			if(x+i >= 0 && x+i < gridDim.x && y+j >= 0 && y+j < gridDim.y)
			{
				if((i != 0 || j != 0) && gof1[gindex + j * width + i]) sum++;
			}
		}
	}
	int live = (gof1[gindex] && (sum >= 2 && sum <= 3)) || (!gof1[gindex] && sum == 3) ? 1 : 0;
	gof2[gindex] = live;
}

void updateWithCuda(const GameOfLife &gameOfLife1, GameOfLife &gameOfLife2)
{
	int size = gameOfLife1.width() * gameOfLife1.height() * sizeof(char);
	char *cudaGof1, *cudaGof2;

	cudaMalloc((void **) &cudaGof1, size);
	cudaMalloc((void **) &cudaGof2, size);

	cudaMemcpy(cudaGof1, gameOfLife1.data(), size, cudaMemcpyHostToDevice);

	dim3 dimBlock(gameOfLife1.width(), gameOfLife1.height(), 1);
	dim3 dimThread(1, 1, 1);

	update_game_of_life_kernel<<<dimBlock, 1>>>(cudaGof1, cudaGof2);

	checkCUDAError("kernel failed!");

	cudaMemcpy(gameOfLife2.data(), cudaGof2, size, cudaMemcpyDeviceToHost);

	cudaFree(cudaGof1);
	cudaFree(cudaGof2);
}

__global__ void render_kernel(uchar4 *buffer, char *cudaGameOfLife)
{
	int index = blockIdx.y * gridDim.x + blockIdx.x;
	//unsigned int width = blockDim.x;
	//unsigned int height = blockDim.y;

	unsigned int alive = cudaGameOfLife[index] ? 0xff : 0x00;
	buffer[index].w = 0x00;
	buffer[index].x = alive;
	buffer[index].y = alive;
	buffer[index].z = alive;
}


void runRenderKernel(uchar4 *data, char *cudaGameOfLife, int width, int height)
{
	dim3 dimBlock(width, height, 1);
	dim3 dimThread(1, 1, 1);

	render_kernel<<<dimBlock, dimThread>>>(data, cudaGameOfLife);

	cudaThreadSynchronize();

	checkCUDAError("kernel failed!");
}

void runUpdateKernel(char *cudaGof1, char *cudaGof2, int width, int height)
{
	dim3 dimBlock(width, height, 1);
	dim3 dimThread(1, 1, 1);

	update_game_of_life_kernel<<<dimBlock, 1>>>(cudaGof1, cudaGof2);

	cudaThreadSynchronize();

	checkCUDAError("kernel failed!");
}


