//
// Created by clement on 26/03/16.
//
#include "GameOfLife.hpp"

#include <stdlib.h>

GameOfLife::GameOfLife(int width, int height)
{
    m_width = width;
    m_height = height;
    int size = m_width * m_height;
    m_map = new char[size];

    for(int x=0; x<size; ++x)
    {
        m_map[x] = 0;
    }
}

GameOfLife::~GameOfLife()
{
    delete[] m_map;
    m_map = NULL;
}

void GameOfLife::randomize(const IRandomNumberGenerator &randomNumberGenerator)
{
    int size = m_width * m_height;
    for(int x=0; x<size; ++x)
    {
        m_map[x] = randomNumberGenerator.pick(2);
    }
}

void GameOfLife::set(int x, int y, int value)
{
    int index = y * m_width + x;
    m_map[index] = value;
}

int GameOfLife::get(int x, int y) const
{
    int index = y * m_width + x;
    return m_map[index];
}

