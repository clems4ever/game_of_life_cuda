#ifndef GAME_OF_LIFE_HPP
#define GAME_OF_LIFE_HPP

#include "../Utils/IRandomNumberGenerator.hpp"

class GameOfLife
{
public:
	GameOfLife(int width, int height);
	virtual ~GameOfLife();

	void randomize(const IRandomNumberGenerator &randomNumberGenerator);

	inline char *data() const
	{
		return m_map;
	}

	void set(int x, int y, int value);
	int get(int x, int y) const;

	inline int width() const
	{
		return m_width;
	}

	inline int height() const
	{
		return m_height;
	}

private:
	int m_width;
	int m_height;
	char *m_map;
};

#endif

