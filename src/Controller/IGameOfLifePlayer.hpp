#ifndef IGAME_OF_LIFE_UPDATER_HPP
#define IGAME_OF_LIFE_UPDATER_HPP

#include "IGameOfLifeRenderer.hpp"

class IGameOfLifePlayer
{
public:
	virtual ~IGameOfLifePlayer()
	{
	}

	virtual unsigned int width() const = 0;
	virtual unsigned int height() const = 0;

	virtual void update() = 0;
	virtual void randomize() = 0;

	virtual IGameOfLifeRenderer &renderer() = 0;

private:
};

#endif

