//
// Created by clement on 27/03/16.
//

#include "GameOfLifeFullGpuRenderer.hpp"

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

template<typename Cuda, typename RenderKernel>
GameOfLifeFullGpuRenderer<Cuda, RenderKernel>::GameOfLifeFullGpuRenderer(int width, int height, char **gameOfLifeBuffer)
: m_gameOfLifeBuffer(gameOfLifeBuffer), m_bufferID(0), m_textureID(0), m_width(width), m_height(height)
{
}

template<typename Cuda, typename RenderKernel>
GameOfLifeFullGpuRenderer<Cuda, RenderKernel>::~GameOfLifeFullGpuRenderer()
{

}

template<typename Cuda, typename RenderKernel>
void GameOfLifeFullGpuRenderer<Cuda, RenderKernel>::initialize()
{
    createPBO(&m_bufferID, m_width, m_height);
    createTexture(&m_textureID, m_width, m_height);
}

template<typename Cuda, typename RenderKernel>
void GameOfLifeFullGpuRenderer<Cuda, RenderKernel>::cleanup()
{
    deleteTexture(&m_textureID);
    deletePBO(&m_bufferID);
}

template<typename Cuda, typename RenderKernel>
void GameOfLifeFullGpuRenderer<Cuda, RenderKernel>::render()
{
    uchar4 *dptr=NULL;

    Cuda::mapBufferObject((void**)&dptr, m_bufferID);

    RenderKernel::run(dptr, *m_gameOfLifeBuffer, m_width, m_height);
    Cuda::unmapBufferObject(m_bufferID);

    // Create a texture from the buffer
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_bufferID);

    // bind texture from PBO
    glBindTexture(GL_TEXTURE_2D, m_textureID);

    // Note: NULL indicates the data resides in device memory
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_width, m_height, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

    glBegin(GL_QUADS);
    glTexCoord2f(0.0f,1.0f); glVertex3f(0.0f,0.0f,0.0f);
    glTexCoord2f(0.0f,0.0f); glVertex3f(0.0f,1.0f,0.0f);
    glTexCoord2f(1.0f,0.0f); glVertex3f(1.0f,1.0f,0.0f);
    glTexCoord2f(1.0f,1.0f); glVertex3f(1.0f,0.0f,0.0f);
    glEnd();

    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glFlush();
}

template<typename Cuda, typename RenderKernel>
void GameOfLifeFullGpuRenderer<Cuda, RenderKernel>::createPBO(GLuint* pbo, unsigned int image_width, unsigned int image_height)
{
    int num_texels = image_width * image_height;
    int num_values = num_texels * 4;
    int size_tex_data = sizeof(GLubyte) * num_values;

    glGenBuffers(1, pbo);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, *pbo);

    // Allocate data for the buffer. 4-channel 8-bit image
    glBufferData(GL_PIXEL_UNPACK_BUFFER, size_tex_data, NULL, GL_DYNAMIC_COPY);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    Cuda::registerBufferObject(*pbo);
}

template<typename Cuda, typename RenderKernel>
void GameOfLifeFullGpuRenderer<Cuda, RenderKernel>::deletePBO(GLuint* pbo)
{
    // unregister this buffer object with CUDA
    Cuda::unregisterBufferObject(*pbo);

    glBindBuffer(GL_ARRAY_BUFFER, *pbo);
    glDeleteBuffers(1, pbo);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

template<typename Cuda, typename RenderKernel>
void GameOfLifeFullGpuRenderer<Cuda, RenderKernel>::createTexture(GLuint* textureID, unsigned int width, unsigned int height)
{
    glEnable(GL_TEXTURE_2D);
    glGenTextures(1,textureID);

    glBindTexture(GL_TEXTURE_2D, *textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, NULL);

    // Must set the filter mode, GL_LINEAR enables interpolation when scaling
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, 0);
}

template<typename Cuda, typename RenderKernel>
void GameOfLifeFullGpuRenderer<Cuda, RenderKernel>::deleteTexture(GLuint* textureID)
{
    glBindTexture(GL_TEXTURE_2D, *textureID);
    glDeleteTextures(1, textureID);
    glBindTexture(GL_TEXTURE_2D, 0);
    *textureID = 0;
}
