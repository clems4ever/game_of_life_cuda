//
// Created by clement on 27/03/16.
//

#include "StudioController.hpp"

#include "Utils/CudaKernelCompiler.hpp"
#include "Utils/DynamicCudaUpdateKernel.hpp"

StudioController::StudioController(std::unique_ptr<ICudaGameOfLifePlayer> gameOfLifePlayer, const std::string &defaultEditorSourceCode)
: m_gameOfLifePlayer(std::move(gameOfLifePlayer))
{
    m_studioWidget = new StudioWidget(m_gameOfLifePlayer->renderer(), defaultEditorSourceCode);

    m_timer = new QTimer();
    m_playPause = PAUSE;
    m_studioWidget->playerControllerWidget().setNextIterationButtonEnabled(true);

    connect(&m_studioWidget->playerControllerWidget(), SIGNAL(onNextIterationButtonClicked()), this, SLOT(computeNextIteration()));
    connect(&m_studioWidget->playerControllerWidget(), SIGNAL(onPlayPauseButtonClicked()), this, SLOT(playPause()));
    connect(&m_studioWidget->playerControllerWidget(), SIGNAL(onRandomizeButtonClicked()), this, SLOT(randomize()));
    connect(&m_studioWidget->playerControllerWidget(), SIGNAL(onApplyKernelButtonClicked()), this, SLOT(runDynamicKernel()));

    connect(m_timer,SIGNAL(timeout()), this, SLOT(computeNextIteration()));
}

StudioController::~StudioController()
{
    delete m_studioWidget;
    delete m_timer;
}

void StudioController::start()
{
    m_studioWidget->resizeSurface(m_gameOfLifePlayer->width(), m_gameOfLifePlayer->height());
    m_studioWidget->updateSurface();
    m_studioWidget->show();
}

void StudioController::computeNextIteration()
{
    m_gameOfLifePlayer->update();
    m_studioWidget->updateSurface();
}

void StudioController::playPause()
{
    if(m_playPause == PAUSE)
    {
        m_timer->start(100);
        m_playPause = PLAY;
        m_studioWidget->playerControllerWidget().setNextIterationButtonEnabled(false);
    }
    else if(m_playPause == PLAY)
    {
        m_timer->stop();
        m_playPause = PAUSE;
        m_studioWidget->playerControllerWidget().setNextIterationButtonEnabled(true);
    }
}

void StudioController::randomize()
{
    m_gameOfLifePlayer->randomize();
    m_studioWidget->updateSurface();
}

void StudioController::runDynamicKernel()
{
    std::unique_ptr<ICudaUpdateKernel> kernel = m_studioWidget->getKernelEditorWidget().kernel()->clone();

    if(kernel)
    {
        m_gameOfLifePlayer->setUpdateKernel(std::move(kernel));
    }
}
