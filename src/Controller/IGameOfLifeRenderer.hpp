//
// Created by clement on 27/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_IRENDERER_HPP
#define GAME_OF_LIFE_CUDA_IRENDERER_HPP

class IGameOfLifeRenderer
{
public:
    virtual void render() = 0;

    virtual void initialize() {};
    virtual void cleanup() {};
};

#endif //GAME_OF_LIFE_CUDA_IRENDERER_HPP
