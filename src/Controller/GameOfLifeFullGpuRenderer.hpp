//
// Created by clement on 27/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_GAMEOFLIFEFULLGPURENDERER1_HPP
#define GAME_OF_LIFE_CUDA_GAMEOFLIFEFULLGPURENDERER1_HPP

#include "IGameOfLifeRenderer.hpp"

#include "GL/gl.h"

template<typename Cuda, typename RenderKernel>
class GameOfLifeFullGpuRenderer : public IGameOfLifeRenderer
{
public:
    GameOfLifeFullGpuRenderer(int with, int height, char **gameOfLifeBuffer);
    virtual ~GameOfLifeFullGpuRenderer();

    void initialize();
    void cleanup();

    void render();

private:
    void createPBO(GLuint* pbo, unsigned int image_width, unsigned int image_height);
    void deletePBO(GLuint* pbo);

    void createTexture(GLuint* textureID, unsigned int width, unsigned int height);
    void deleteTexture(GLuint* tex);

private:
    int m_width;
    int m_height;

    GLuint m_bufferID;
    GLuint m_textureID;

    char **m_gameOfLifeBuffer;
};

#include "GameOfLifeFullGpuRenderer.inl"

#endif //GAME_OF_LIFE_CUDA_FULLGPURENDERER_HPP
