#ifndef GAME_OF_LIFE_UPDATER_FULL_GPU_HPP
#define GAME_OF_LIFE_UPDATER_FULL_GPU_HPP

#include "Model/GameOfLife.hpp"
#include "ICudaGameOfLifePlayer.hpp"
#include "GameOfLifeFullGpuRenderer.hpp"

#include <GL/gl.h>
#include "Utils/CudaTypes.hpp"
#include "Utils/ICudaUpdateKernel.hpp"

#include <memory>

template<typename Cuda, typename RenderKernel>
class GameOfLifeUpdaterFullGpu : public ICudaGameOfLifePlayer
{
public:
	GameOfLifeUpdaterFullGpu(int width, int height, int cellSize);
	virtual ~GameOfLifeUpdaterFullGpu();

    unsigned int width() const;
    unsigned int height() const;

    void update();
    void randomize();

    IGameOfLifeRenderer& renderer()
    {
        return m_renderer;
    }

private:
	int m_cellSize;
	int m_width;
	int m_height;

	GameOfLife m_gameOfLife;

    char *m_cudaGameOfLife1;
    char *m_cudaGameOfLife2;

    char *m_currentGameOfLife;
    char *m_bufferGameOfLife;

    GameOfLifeFullGpuRenderer<Cuda, RenderKernel> m_renderer;
};

#include "GameOfLifeUpdaterFullGpu.inl"

#endif

