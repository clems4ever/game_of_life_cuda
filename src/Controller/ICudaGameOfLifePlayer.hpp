//
// Created by clement on 28/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_ICUDAGAMEOFLIFEPLAYER_HPP
#define GAME_OF_LIFE_CUDA_ICUDAGAMEOFLIFEPLAYER_HPP

#include "Controller/IGameOfLifePlayer.hpp"

#include "Utils/ICudaUpdateKernel.hpp"

class ICudaGameOfLifePlayer : public IGameOfLifePlayer
{
public:
    virtual ~ICudaGameOfLifePlayer() {}

    void setUpdateKernel(std::unique_ptr<ICudaUpdateKernel> f)
    {
        m_updateKernel = std::move(f);
    }

protected:
    std::unique_ptr<ICudaUpdateKernel> m_updateKernel;
};

#endif //GAME_OF_LIFE_CUDA_ICUDAGAMEOFLIFEPLAYER_HPP
