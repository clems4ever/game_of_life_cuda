//
// Created by clement on 27/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_STUDIOCONTROLLER_HPP
#define GAME_OF_LIFE_CUDA_STUDIOCONTROLLER_HPP

#include <memory>

#include "types.hpp"

#include "ICudaGameOfLifePlayer.hpp"
#include "GameOfLifeUpdaterFullGpu.hpp"

#include "View/StudioWidget.hpp"

class StudioController : public QObject
{
    Q_OBJECT

public:
    StudioController(std::unique_ptr<ICudaGameOfLifePlayer> gameOfLifePlayer, const std::string &defaultEditorSourceCode);
    virtual ~StudioController();

    void start();

public slots:
    void computeNextIteration();
    void playPause();
    void randomize();

    void runDynamicKernel();

private:
    std::unique_ptr<ICudaGameOfLifePlayer> m_gameOfLifePlayer;
    StudioWidget *m_studioWidget;

    QTimer *m_timer;
    enum PlayPause {
        PLAY,
        PAUSE
    } m_playPause;
};


#endif //GAME_OF_LIFE_CUDA_CONTROLLER_HPP
