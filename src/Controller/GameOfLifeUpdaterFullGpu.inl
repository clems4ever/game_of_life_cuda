//
// Created by clement on 27/03/16.
//
#include "GameOfLifeUpdaterFullGpu.hpp"

#include <GL/gl.h>
#include "GL/freeglut.h"

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include "../Utils/RandomNumberGenerator.hpp"

template<typename Cuda, typename RenderKernel>
GameOfLifeUpdaterFullGpu<Cuda, RenderKernel>::GameOfLifeUpdaterFullGpu(int width, int height, int cellSize)
        : m_gameOfLife(width, height), m_width(width), m_height(height), m_renderer(width, height, &m_currentGameOfLife),
          m_cellSize(cellSize), m_cudaGameOfLife1(NULL), m_cudaGameOfLife2(NULL), m_currentGameOfLife(NULL), m_bufferGameOfLife(NULL)
{
    RandomNumberGenerator generator;
    m_gameOfLife.randomize(generator);

    int size = m_width * m_height * sizeof(char);

    Cuda::malloc((void **) &m_cudaGameOfLife1, size);
    Cuda::malloc((void **) &m_cudaGameOfLife2, size);

    Cuda::memcpy(m_cudaGameOfLife1, m_gameOfLife.data(), size, cudaMemcpyHostToDevice);

    m_currentGameOfLife = m_cudaGameOfLife1;
    m_bufferGameOfLife = m_cudaGameOfLife2;
}

template<typename Cuda, typename RenderKernel>
GameOfLifeUpdaterFullGpu<Cuda, RenderKernel>::~GameOfLifeUpdaterFullGpu()
{
    Cuda::free(m_cudaGameOfLife1);
    Cuda::free(m_cudaGameOfLife2);

    m_cudaGameOfLife1 = NULL;
    m_cudaGameOfLife2 = NULL;
    m_currentGameOfLife = NULL;
    m_bufferGameOfLife = NULL;
}

template<typename Cuda, typename RenderKernel>
unsigned int GameOfLifeUpdaterFullGpu<Cuda, RenderKernel>::width() const
{
    return m_gameOfLife.width();
}

template<typename Cuda, typename RenderKernel>
unsigned int GameOfLifeUpdaterFullGpu<Cuda, RenderKernel>::height() const
{
    return m_gameOfLife.height();
}

template<typename Cuda, typename RenderKernel>
void GameOfLifeUpdaterFullGpu<Cuda, RenderKernel>::update()
{
    CudaDim3 blocks = {m_width, m_height, 1};
    CudaDim3 threads = {1, 1, 1};

    m_updateKernel->run(blocks, threads, m_currentGameOfLife, m_bufferGameOfLife);

    if(m_currentGameOfLife == m_cudaGameOfLife1)
    {
        m_currentGameOfLife = m_cudaGameOfLife2;
        m_bufferGameOfLife = m_cudaGameOfLife1;
    }
    else
    {
        m_currentGameOfLife = m_cudaGameOfLife1;
        m_bufferGameOfLife = m_cudaGameOfLife2;
    }
}

template<typename Cuda, typename RenderKernel>
void GameOfLifeUpdaterFullGpu<Cuda, RenderKernel>::randomize()
{
    RandomNumberGenerator randomNumberGenerator;
    m_gameOfLife.randomize(randomNumberGenerator);

    int size = m_width * m_height * sizeof(char);
    Cuda::memcpy(m_currentGameOfLife, m_gameOfLife.data(), size, cudaMemcpyHostToDevice);
}


