//
// Created by clement on 27/03/16.
//

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "Model/GameOfLife.hpp"
#include "MockRandomNumberGenerator.hpp"

using namespace ::testing;

TEST(GameOfLifeTest, initialization_must_set_height_and_width_and_initialize_data)
{
    GameOfLife gameOfLife(10, 20);

    ASSERT_EQ(10, gameOfLife.width());
    ASSERT_EQ(20, gameOfLife.height());
    ASSERT_TRUE(gameOfLife.data() != NULL);
}

TEST(GameOfLifeTest, initialization_must_set_data_to_zero)
{
    GameOfLife gameOfLife(10, 20);

    for(unsigned int x=0; x<10; ++x)
    {
        for(unsigned int y=0; y<20; ++y)
        {
            ASSERT_EQ(0, gameOfLife.get(x, y));
        }
    }
}

TEST(GameOfLifeTest, set_and_get_a_value)
{
    GameOfLife gameOfLife(10, 15);

    gameOfLife.set(3, 4, 0);

    ASSERT_EQ(0, gameOfLife.get(3, 4));

    gameOfLife.set(3, 4, 34);

    ASSERT_EQ(34, gameOfLife.get(3, 4));
}

TEST(GameOfLifeTest, randomize_data)
{
    GameOfLife gameOfLife(10, 20);
    NiceMock<MockRandomNumberGenerator> randomNumberGenerator;

    ON_CALL(randomNumberGenerator, pick(2)).WillByDefault(::testing::Return(1));

    gameOfLife.randomize(randomNumberGenerator);

    for(unsigned int x=0; x<10; ++x)
    {
        for(unsigned int y=0; y<20; ++y)
        {
            ASSERT_EQ(1, gameOfLife.get(x, y));
        }
    }
}

