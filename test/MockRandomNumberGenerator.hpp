//
// Created by clement on 27/03/16.
//

#ifndef GAME_OF_LIFE_CUDA_MOCKRANDOMNUMBERGENERATOR_HPP
#define GAME_OF_LIFE_CUDA_MOCKRANDOMNUMBERGENERATOR_HPP

#include <gmock/gmock.h>

#include "Utils/IRandomNumberGenerator.hpp"

class MockRandomNumberGenerator : public IRandomNumberGenerator
{
public:
    MOCK_CONST_METHOD1(pick, int (int max_value));
};

#endif //GAME_OF_LIFE_CUDA_MOCKRANDOMNUMBERGENERATOR_HPP
