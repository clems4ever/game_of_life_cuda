cmake_minimum_required(VERSION 3.4)
project(game_of_life_cuda)

SET(TARGET game_of_life_cuda_unit_test)

set(CUDA_HOST_COMPILER /usr/bin/g++-4.9)
find_package(CUDA QUIET REQUIRED)

enable_testing()
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES GameOfLifeTest.cpp)

include_directories(${CMAKE_SOURCE_DIR}/src)

#cuda_add_executable(${TARGET} ${SOURCE_FILES})
#target_link_libraries(${TARGET} -pthread -lgtest -lgtest_main -lgmock lib_game_of_life_cuda)